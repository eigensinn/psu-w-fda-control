import sys
import os
from serial import Serial
from serial.serialutil import SerialException
from time import sleep
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5.QtCore import QSettings
from ui_main import Ui_MainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.hs_power.setMaximum(7480)
        
        try:
            settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), \
                                              "settings.ini"), QSettings.IniFormat)
            self.ui.hs_power.setValue(int(settings.value("hs_power")))
            self.ui.le_power.setText(settings.value("le_power"))
            self.ui.combo_baud.setCurrentText(settings.value("combo_baud"))
            self.ui.le_port.setText(settings.value("le_port"))
            self.restoreState(settings.value("windowstate"))
            self.restoreGeometry(settings.value("geo"))
            
        # Если программа запускается в первый раз и настройки еще не сохранялись
        except TypeError:
            pass

        self.ui.pb_off.clicked.connect(lambda: self.send_message(0))
        self.ui.pb_on.clicked.connect(lambda: self.send_message(1))
        self.ui.pb_power.clicked.connect(lambda: self.send_message(2))
        self.ui.le_port.textChanged.connect(self.save_settings)
        self.ui.le_power.textChanged.connect(self.save_settings)
        self.ui.le_power.textChanged.connect(self.hs_reaction)
        self.ui.hs_power.valueChanged.connect(self.le_power_reaction)
        self.ui.hs_power.valueChanged.connect(self.save_settings)
        self.ui.combo_baud.currentIndexChanged.connect(self.save_settings)
        self.ui.act_about.triggered.connect(self.about)

    def hs_reaction(self):
        val_str = self.ui.le_power.text()
        if val_str.isdigit():
            val = int(val_str)
            if val > 7480:
                val = 7480
                self.ui.le_power.setText("7480")
            self.ui.hs_power.setValue(val)

    def le_power_reaction(self):
        self.ui.le_power.setText(str(self.ui.hs_power.value()))

    # Событие закрытия окна. Сохраняем размеры и состояние виджетов
    def closeEvent(self, event):
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                             QSettings.IniFormat)
        arr = [("windowstate", self.saveState()), ("geo", self.saveGeometry())]
        for i, j in arr:
            settings.setValue(i, j)
        QMainWindow.closeEvent(self, event)

    # Сохраняем состояния виджетов
    def save_settings(self):
        settings = QSettings(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "settings.ini"),
                             QSettings.IniFormat)
        arr = []
        arr.append(("hs_power", self.ui.hs_power.value()))
        arr.append(("le_power", self.ui.le_power.text()))
        arr.append(("le_port", self.ui.le_port.text()))
        arr.append(("combo_baud", self.ui.combo_baud.currentText()))
        for i, j in arr:
            settings.setValue(i, j)

    def write(self, ser, message):
        if ser.is_open:
            ser.flushInput()
            ser.flushOutput()
            sleep(0.1)
            try:
                ser.write(message)
            except Exception as exc:
                print('type: {0}, message: {1}'.format(type(exc), str(exc)))
            else:
                res = ser.readline()
                print(res)
                self.ui.textEdit.append(str(res))
                ser.close()

    # Checking length: odd or even
    def checking_length(self, arr):
        for i, bit in enumerate(arr):
            if not len(bit) % 2 == 0:
                arr[i] = '0' + arr[i]
        # Если не влезает в 4 знака, убираем первую строку и делим вторую пополам
        if len(arr[1]) == 4:
            arr[0] = arr[1][:2]
            arr[1] = arr[1][2:4]
        return arr
            
    def send_message(self, v):
        if v == 0:
            hex_string = '55 aa 03 00 03'
            message = bytes.fromhex(hex_string)
        elif v == 1:
            hex_string = '55 aa 03 01 04'
            message = bytes.fromhex(hex_string)
        elif v == 2:
            real_power = int(self.ui.hs_power.value())
            # Переводим значение из желаемой фактической мощности в значения для источника питания лазера
            driver_power = 1.348*real_power - 86.4
            bit1and2 = self.checking_length(hex(driver_power).split('x'))
            try:
                final_hex = ['55', 'aa', '04', bit1and2[0], bit1and2[1]]
                message = bytes.fromhex(''.join(final_hex))
                hex_string = ' '.join(final_hex)
            except Exception as exc:
                print('type: {0}, message: {1}'.format(type(exc), str(exc)))
        self.ui.textEdit.append(hex_string)
        try:
            ser = Serial(port='COM'+self.ui.le_port.text(), baudrate=int(self.ui.combo_baud.currentText()), timeout = 0.1)
        except SerialException as exc:
            self.ui.textEdit.append('type: {0}, message: {1}'.format(type(exc), str(exc)))
        else:
            try:
                ser.open()
            except SerialException as exc:
                print('type: {0}, message: {1}'.format(type(exc), str(exc)))
                self.write(ser, message)
            else:
                self.write(ser, message)

#-----------------------------------------------------------------------
        
    def about(self):
        QMessageBox.about(self, "О psu-w-fda-control",
                                "psu-w-fda-control версия 1.1.2\nДата сборки: 05.03.2019\nНа основе библиотеки PyQt5\n")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('sgi')
    app.setStyleSheet(open(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), "style.qss"), "r").read())
    mw = MainWindow()
    mw.show()
    sys.exit(app.exec_())
