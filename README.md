# CNI Laser MGL-W-532-HQ-10W control with PSU-W-FDA power supply

The application is written based on PyQt5 with QtDesigner.

## Convert ui to py

```bash
path\to\pyuic5 ui_main.ui -o ui_main.py
```

## Power Calibration

From the [power calibration trend line](https://bitbucket.org/eigensinn/psu-w-fda-control/src/master/img/power_calibration.jpg), we find that:

```python
driver_power = 1.348*real_power - 86.4
```

## RS-232 Communication Commands



```python
Enter a decimal power: 55 AA 04 bit1 bit2
Laser off:             55 AA 03 00 03
Laser on:              55 AA 03 01 04

Where (simplified):
bit1, bit2 = hex(driver_power).split('x')

For example, enter 100 mW: 55 AA 04 00 64
```

## License

[MIT License](https://bitbucket.org/eigensinn/psu-w-fda-control/src/master/LICENSE)